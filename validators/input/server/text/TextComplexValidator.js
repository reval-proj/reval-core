
const ANSWER_CODES = {
    VALID: 200,
    NOT_VALID: 400,
    ERROR: 500
};

const SERVER_URL = "http://127.0.0.1:8080/get?value=";

export default class TextComplexValidator {

    async validation(state, str) {
              
        let validObj = {
            isValid: true,
            results: null
        };

        const success = await fetch(SERVER_URL + str).then(function(response) {
            return response.json().then(function(data) {    

                switch(data.code) {
                    case ANSWER_CODES.VALID:
                        validObj.isValid = true;
                        validObj.results = data.tempStr;
                        break;
                    case ANSWER_CODES.NOT_VALID:
                        validObj.isValid = false;
                        break;
                    case ANSWER_CODES.ERROR:
                        validObj.isValid = false;
                        console.log("Error on server.");
                        break;
                    }
                return validObj;
            });
        }).catch(function(ex) {
            console.log('Failed get data from server', ex)
            return validObj;
        });

        return success;
    }
}