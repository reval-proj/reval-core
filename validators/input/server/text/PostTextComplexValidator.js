
export default class PostTextComplexValidator {

    doAction(changeData, state, validObj) {
        let newChangeData = {
            value: "",
            style: {},
            hideData: {}
        }

        if(validObj.results != null) {
            newChangeData.hideData = validObj.results;
        }
        newChangeData.value = changeData.value;
        newChangeData.style = changeData.style;

        return newChangeData;
    }
}