
const DEFAULT_PROPS = {
    type: "text",
    mode: "ld", 
    adSet: {}, 
    validate: "notInput", 
    length: 20,
    value: "",
    style: {},
    className: "rSearchInput",
    errorClassName: "rSearchInput-notValid"
}

export default class PreTextComplexValidator {

    getProperties(props) {
        return ({
            type: props.type || DEFAULT_PROPS.type,
            mode: props.mode || DEFAULT_PROPS.mode, 
            adSet: props.adSet || DEFAULT_PROPS.adSet, 
            validate: props.validate || DEFAULT_PROPS.validate, 
            length: props.length || DEFAULT_PROPS.length,
            value: props.value || DEFAULT_PROPS.value,
            style: props.style || DEFAULT_PROPS.style,
            className: DEFAULT_PROPS.className,
            errorClassName: DEFAULT_PROPS.errorClassName
        })
    }
}