/*
*   
*/


const DEFAULT_PROPS = {
    type: "date",
    validate: "notInput",
    style: {},
    options: {},
    value: "",
    extraParams: {
        min: "",
        max: "",
        step: "1"
    },
    className: "rDateInput",
    errorClassName: "rDateInput-notValid"
}

export default class PreDateValidator {

    getProperties(props) {
        console.log(DEFAULT_PROPS);
        return ({
            type: props.type || DEFAULT_PROPS.type,
            validate: props.validate || DEFAULT_PROPS.validate,
            style: props.style || DEFAULT_PROPS.style,
            options: props.options || DEFAULT_PROPS.options,
            value: props.value || DEFAULT_PROPS.value,
            extraParams: props.extraParams || DEFAULT_PROPS.extraParams,
            className: DEFAULT_PROPS.className,
            errorClassName: DEFAULT_PROPS.errorClassName
        });
    }
}