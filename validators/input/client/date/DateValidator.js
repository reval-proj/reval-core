
export default class DateVilator {

    validation(state, date) {

        let isValid = this.isDateLessThanMax(date, state.extraParams.max)
                && this.isDateLargerThanMin(date, state.extraParams.min);

        let validObj = {
            isValid: isValid
        }

        return validObj;
    }

    isDateLessThanMax(date, maxDate) {
        if(maxDate == undefined || maxDate == null || maxDate === "") {
            return true;
        }

        return Date.parse(date) <= Date.parse(maxDate);
    }

    isDateLargerThanMin(date, minDate) {
        if(minDate == undefined || minDate == null || minDate === "") {
            return true;
        }

        return Date.parse(date) >= Date.parse(minDate);
    }
}