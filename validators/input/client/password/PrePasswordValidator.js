
const DEFAULT_PROPS = {
    type: "password",
    mode: "ld", 
    adSet: {}, 
    validate: "notInput", 
    length: 20,
    value: "",
    validStyle: {},
    errorStyle: {},
    className: "rPasswordInput",
    errorClassName: "rPasswordInput-notValid"
}

export default class PrePasswordValidation {

    getProperties(props) {
        console.log(DEFAULT_PROPS);
        return ({
            type: props.type || DEFAULT_PROPS.type,
            mode: props.mode || DEFAULT_PROPS.mode, 
            adSet: props.adSet || DEFAULT_PROPS.adSet, 
            validate: props.validate || DEFAULT_PROPS.validate, 
            length: props.length || DEFAULT_PROPS.length,
            value: props.value || DEFAULT_PROPS.value,
            style: props.style || DEFAULT_PROPS.style,
            errorStyle: props.errorStyle || DEFAULT_PROPS.errorStyle,
            validStyle: props.validStyle || DEFAULT_PROPS.validStyle,
            className: DEFAULT_PROPS.className,
            errorClassName: DEFAULT_PROPS.errorClassName
        })
    }
}