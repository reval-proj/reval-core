var LETTER_AND_DIGITS = /^[0-9A-Za-zА-Яа-я]*$/;
var LETTER = /^[A-Za-zА-Яа-я]*$/;
var DIGITS = /^[0-9]*$/;
var LD = "^[0-9A-Za-zА-Яа-я";
var SOME_CHARACTERS = /[_@,. ]/;

export default class TextValidator {
    
    hasLittersAndDigits(str) {
        return LETTER_AND_DIGITS.test(str);
    }

    hasLitters(str) {
        return LETTER.test(str);
    }

    hasDigits(str) {
        return DIGITS.test(str);
    }

    hasLDAndSomeChar(str, adSet) {
        if (SOME_CHARACTERS.test(adSet)) {
            return new RegExp(LD + adSet + "]*$").test(str);
        }
        return this.hasLittersAndDigits(str);
    }

    isValideString(str, adSet) {
        return new RegExp(adSet).test(str);
    }

    validation(state, str) {
        let isValid = true;
        switch(state.mode) {
            case "ld":
                isValid = this.hasLittersAndDigits(str)
                break;
            case "l":
                isValid = this.hasLitters(str)
                break;
            case "d":
                isValid = this.hasDigits(str)
                break;
            case "ldc":
                isValid = this.hasLDAndSomeChar(str, state.adSet)
                break;
            case "re":
                isValid = this.isValideString(str, state.adSet)
                break;
        }

        if (state.length != undefined) {
            isValid = isValid && str.length <= state.length;
        }

        let validObj = {
            isValid: isValid,
        };

        return validObj;
    }
}