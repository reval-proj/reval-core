
export default class PreTextValidatior {

    constructor() {
        this.DEFAULT_PROPS = {
            type: "text",
            mode: "ld", 
            adSet: {}, 
            validate: "notInput", 
            length: 20,
            value: "",
            errorStyle: {},
            validStyle: {},
            validClassName: "rTextInput",
            errorClassName: "rTextInput-notValid"
        };
    }

    getProperties(props) {
        return ({
            type: props.type || this.DEFAULT_PROPS.type,
            mode: props.mode || this.DEFAULT_PROPS.mode, 
            adSet: props.adSet || this.DEFAULT_PROPS.adSet, 
            validate: props.validate || this.DEFAULT_PROPS.validate, 
            length: props.length || this.DEFAULT_PROPS.length,
            value: props.value ||this. DEFAULT_PROPS.value,
            style: props.style || this.DEFAULT_PROPS.style,
            errorStyle: props.errorStyle || this.DEFAULT_PROPS.errorStyle,
            validClassName: this.DEFAULT_PROPS.validClassName,
            errorClassName: this.DEFAULT_PROPS.errorClassName
        })
    };
}