export default class PostTextValidator {

    doAction(changeData, state) {
        let newChangeData = {
            value: "",
            style: state.validStyle,
            className: state.validClassName
        };

        switch(state.validate) {
            case "notInput":  
                newChangeData.value = state.value;
                break;
            case "alert":
                newChangeData = this.actionForAlert(changeData, state);
                alert("Don't valide value: " + changeData.value);
                break;
            case "color":
                newChangeData = this.actionForColor(changeData, state);
                break;
            default:
                break;  
        }

        return newChangeData;
    }

    actionForAlert(changeData, state) {
        return {
            value: changeData.value,
            style: state.errorStyle,
            className: state.errorClassName
        }
    }

    actionForColor(changeData, state) {
        return {
            value: changeData.value,
            style: state.errorStyle,
            className: state.errorClassName
        }
    }
}