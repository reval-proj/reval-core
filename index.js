import PreTextValidator from './validators/input/client/text/PreTextValidator'
import TextValidator from './validators/input/client/text/TextValidator'
import PostTextValidator from './validators/input/client/text/PostTextValidator'

import PreDateValidator from './validators/input/client/date/PreDateValidator'
import DateValidator from './validators/input/client/date/DateValidator'
import PostDateValidator from './validators/input/client/date/PostDateValidator'

import PrePasswordValidator from './validators/input/client/password/PrePasswordValidator'
import PasswordValidator from './validators/input/client/password/PasswordValidator'
import PostPasswordValidator from './validators/input/client/password/PostPasswordValidator'

import PreTextComplexValidator from './validators/input/server/text/PreTextComplexValidator'
import TextComplexValidator from './validators/input/server/text/TextComplexValidator'
import PostTextComplexValidator from './validators/input/server/text/PostTextComplexValidator'

export class AbstractValidator {

	getValidators(inputType) {
		let validator;
		switch(inputType) {
			case "text":
				validator = this.getTextValidators();
				break;
			case "date":
				validator = this.getDateValidators();
				break;
			case "password":
				validator = this.getPasswordValidators();
				break;
			case "search":
				validator = this.getTextComplexValidator();
				break;
			default:
				validator = this.getTextValidators();
				break;
		}
		return validator;
	}

	getTextValidators() {
        return {
            preValidator: new PreTextValidator(),
            validator: new TextValidator(),
            postValidator: new PostTextValidator()
        }
	}

	getPasswordValidators() {
		return {
			preValidator: new PrePasswordValidator(),
            validator: new PasswordValidator(),
            postValidator: new PostPasswordValidator()
		}
	}

	getDateValidators() {
		return {
            preValidator: new PreDateValidator(),
            validator: new DateValidator(),
            postValidator: new PostDateValidator()
        }
	}

	getTextComplexValidator() {
		return {
			preValidator: new PreTextComplexValidator(),
			validator: new TextComplexValidator(),
			postValidator: new PostTextComplexValidator()
		}
	}
}